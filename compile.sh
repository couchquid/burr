#!/bin/bash -ex

mkdir lib
cp /usr/share/java/hamcrest* /usr/share/java/junit* lib/
chmod +x lib/*.jar
export CLASSPATH=":./*"
export CLASSPATH="$CLASSPATH:./lib/*"
javac -d . src/*.java
javac -d . test/*.java
